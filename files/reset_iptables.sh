#!/bin/bash

if [ -f "/etc/iptables/rules.v4" ]; then
  mv /etc/iptables/rules.v4 /etc/iptables/rules.v4.bak
fi

if [ -f "/etc/iptables/rules.v6" ]; then
  mv /etc/iptables/rules.v6 /etc/iptables/rules.v6.bak
fi

iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT

iptables -F INPUT
iptables -F OUTPUT
iptables -F FORWARD

touch /etc/iptables/ansible.first.iptables.flush

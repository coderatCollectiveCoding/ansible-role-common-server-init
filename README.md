# Ansible Role: Common Server Initialisation
Configures common server apps and create users:
* Apt config
* [Apticron](https://manpages.debian.org/testing/apticron/apticron.1.en.html)
* [Nullmailer mail relay](https://wiki.archlinux.org/index.php/Nullmailer)
* Message of the day
* Sshd
* [Ufw](https://help.ubuntu.com/community/UFW)
* Locale and timezone
* Install common software
* hostname
* User creation with vim and zsh config


## Role Variables
See defaults at [defaults/main.yml](https://gitlab.com/coderatCollectiveCoding/ansible-role-common-server-init/-/blob/master/defaults/main.yml)


```
user: 
  - { name: 'my_name', password: "{{ myname_vaulted_pw_hash }}", ssh_pub_key: "{{ my_name_vaulted_pub_key }}", dotfiles_git: '' }
```

## Example Playbook
```
- hosts: all
  roles:
    - hansheimlich.common_server_init

```

## License
MIT / BSD

## Author Information
This role was created in 2019 by hansheimlich
